package com.task;
import java.util.Scanner;


public class TaskCh10N044 {
    public static int sumDigits(int a) {
        int sum = 0;

        do {
            sum += a % 10;
            a = a / 10;
        } while (a > 0);

        if (sum >= 10) {
            return sumDigits(sum);
        }
        return sum;
    }


    public static void main (final String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int a = sc.nextInt();

        double sumDigit = sumDigits(a);
        System.out.println("Сумма цифр числа " + sumDigit);

    }

}
