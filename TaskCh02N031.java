package com.task;
import java.util.Scanner;

class TaskCh02N031 {
    public static void main (String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число n от 100 до 999:");
        int n = sc.nextInt();
        int n1 = n / 100;
        int n2 = n / 10 % 10;
        int n3 = n % 10;
        if (n>100&n<999)
            System.out.println("Вы ввеличисло " +n + ". При перестановке чисел по заданному условию x равен " +n1 + n3 +n2);

        else System.out.println("Введенное число не входит в интервал от 100 до 999");

    }
}
