package com.task;
import java.util.ArrayList;
import java.util.List;

interface PrintMessege { //Это обьявление интерфейса PrintMessege, в нем иниациализируется метод hello, применяемый в классах Employer и AbsCandidate
    void hello();//Это объявление строкового метода приветствия

}
//это абстраткный класс Кандидата. Здесь реализуется интерфейс PrintMessege и обьявляется метод describeExperience
abstract class AbsCandidate implements PrintMessege {
    String name;  //обьявление переменной имени

    //обьявление метода describeExperience в абстрактном классе
    public abstract void describeExperience();

    //реализация метода Hello для кандидата
    public void hello() {
        System.out.println("hi! my name is " + name);
    }
}
//это класс Кандидата Self. Здесь реализуется  метод describeExperience из абстрактного класса AbsCandidate
class Self extends AbsCandidate {

    //передача переменной имени кандидата в класс AbsCandidate
    public Self(String name) {
              this.name = name;
    }
    //реализация метода describeExperience для кандидата Self
    @Override
    public void describeExperience() {
        System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code");
    }
}
//это класс Кандидата GetJavaJob. Здесь реализуется  метод describeExperience из абстрактного класса AbsCandidate
class GetJavaJob extends AbsCandidate {
    //передача переменной имени кандидата в класс AbsCandidate
    public GetJavaJob(String name) {
        this.name = name;
    }

    //реализация метода describeExperience для кандидата GetJavaJob
    @Override
    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews");
    }
}

//это класс нанимателя. Здесь реализуется интерфейс PrintMessege
class Employer implements PrintMessege {//класс Employer реализует интерфейс Printmessege

    //Это реализация нанимателем метода hello, получаемого из интерфейса PrintMessege
    @Override
    public void hello() { //реализован метод hello
        System.out.println("hi! Introduce yourself and describe your java experience please");
    }
}

public class OOP_Task_1 {

    public static void main(String[] args) {
        //создание экземпляра класса Employers для использования метода hello из класса Employers
        Employer employer = new Employer();


        //создание списка кандидатов обьектов класса absCandidate
        List<AbsCandidate> candidates = new ArrayList<>();

        candidates.add(new Self("Ivan"));
        candidates.add(new GetJavaJob("Andrey"));
        candidates.add(new Self("Vasiliy"));
        candidates.add(new GetJavaJob("Petr"));
        candidates.add(new Self("Misha"));
        candidates.add(new GetJavaJob("Nikita"));
        candidates.add(new Self("Marat"));
        candidates.add(new GetJavaJob("Kostya"));
        candidates.add(new Self("Semen"));
        candidates.add(new GetJavaJob("Oleg"));

        //создание цикла из обьектов класса AbsCandidate в виде диалога
        for (AbsCandidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();

            System.out.println("__________");
        }
    }
}