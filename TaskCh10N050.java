package com.task;

import java.lang.*;

public class TaskCh10N050_1 {
    public static long ackermann(long m, long n) {
        if (m == 0) {
            return n + 1;
        }
        if (m > 0 && n == 0) {
            return ackermann(m - 1, 1);
        }
        if (m > 0 && n > 0) {
            return ackermann(m - 1, ackermann(m, n - 1));
        }

        System.out.println("Внимание: m и n не могут быть отрицательными");
        return -1;
    }
    public static void main(String[] args) {
        long m = new Long("2");
        long n = new Long("4");

        System.out.println(ackermann(m, n));

    }
}
