package com.task;
import java.util.Scanner;
import java.lang.*;

class Calculation
{
    int fact(int n)
    {
        int result;

        if(n==1)
            return 1;

        result = fact(n-1) * n;
        return result;
    }
}

public class TaskCh10N041 {

        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Введите число");
            int i = sc.nextInt();


            Calculation obj_one = new Calculation();

            int a = obj_one.fact(i);
            System.out.println("The factorial of the number is : " + a);
                }
            }


