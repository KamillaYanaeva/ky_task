package com.task;
import java.util.Scanner;

public class TaskCh04N106 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите номер месяца");
        int m = sc.nextInt();
       
            String season;
            switch (m) {
                case 1:
                case 2:
                case 12:
                    season = "Зима";
                    break;

                case 3:
                case 4:
                case 5:
                    season = "Весна";
                    break;

                case 6:
                case 7:
                case 8:
                    season = "Лето";
                    break;

                case 9:
                case 10:
                case 11:
                    season = "Осень";
                    break;
                default:
                    season = "Номер месяца введен некорректно";
            }
            System.out.println(season);
       // }

    }
}


