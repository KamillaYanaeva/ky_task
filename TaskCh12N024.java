package com.task;

import java.util.Scanner;

public class TaskCh12N024 {

    public static void main(String[] args) {
        System.out.println("Введите размер матрицы:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[][] matrix = new int[n][n];
//задание а)
        //заполнить 0 строку и 0 столбец единицами
        for( int i = 0; i < n; i++ )
        {
            matrix[i][0] = matrix[0][i] = 1;
        }
        // Заполнить с 1 по n строку и с 1 по n столбец
        for(int i = 1; i < n; i++)
        {
           for(int j = 1; j < n; j++)
           {
               matrix[i][j] = matrix[i][j -1] + matrix[i -1][j];
              if( i != j )
              {
                  matrix[i][j] = matrix[i][j];
              }
           }

       }

        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }


        System.out.println("_______________________\n");

 //задание б)
      int x=0;
      for (int i = 0; i < n; i++) {
          x++;
          for (int j = 0; j < n; j++) {
            matrix[i][j] = x++;
               if (x > n) x = 1;
              System.out.print(matrix[i][j] + " ");
           }
            System.out.println();
       } System.out.println("_______________________\n");
    }
}
