package com.task;
import java.util.Random;

public class TaskCh12N063 {

    public static void main(String[] args) {
        int n = 11;
        int m = 4;
        int[][] matrix = new int[n][m];
        int x=0;
        int y=0;
        for (int i=0;i < n;i++){
            for (int j=0;j < m;j++){
                matrix[i][j]=(int)(20+Math.random()*(35-20));
            }
        }
        System.out.print("  класс          |           параллели           |   среднее кол-во учеников в параллели\n");
        System.out.print("---------------------------------------------------------------------------------------\n");
        for (int i=0;i < n;i++) {
            System.out.println();
            System.out.print(i+1+"-е классы   \t ");

            for (int j=0;j < m;j++){
                y =y+matrix[i][j];
                System.out.print("| "+matrix[i][j]+"\t " );

            }

            System.out.print("|          " + y/m );
            y=0;
        }
    }
}
