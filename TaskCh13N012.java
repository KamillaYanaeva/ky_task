package com.task;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


class Employee {
    public String surname;
    public String name;
    public String middlename;
    public String address;
    public int year;
    public int month;

    Employee(String surname, String name, String middlename, String address, int month, int year) {
        this.name = name;
        this.middlename = middlename;
        this.surname = surname;
        this.address = address;
        this.year = year;
        this.month = month;
    }

    public String toString() {
    return String.format("Имя: %s, Фамилия: %s, Отчество: %s, Город: %s, Год: %d, Месяц: %d\n---------------------------",
    name, middlename, surname, address, year, month);
    }
}
        public class TaskCh13N012 {

            public static void main(String[] args) {

                ArrayList<Employee> dataBase  = new ArrayList<Employee>();


        dataBase.add(new Employee("Иванов", "Альберт", "Петрович", "Омск", 7, 2020));
        dataBase.add(new Employee("Сидоров", "Сидор", "Сидорович", "Урюпинск", 9, 1983));
        dataBase.add(new Employee("Петров", "Антон", "Юсуфович", "Москва", 11, 2004));
        dataBase.add(new Employee("Баканов", "Виталий", "Юрьевич", "Москва", 12, 2020));
        dataBase.add(new Employee("Белышев", "Михаил", "Олегович", "Москва", 06, 2004));
        dataBase.add(new Employee("Смирнов", "Дмитрий", "Иванович", "Набережные Челны", 11, 2004));
        dataBase.add(new Employee("Никитков", "Олег", "Юрьевич", "Зеленоград", 05, 2020));
        dataBase.add(new Employee("Сорокин", "Никита", "Михайлович", "Астрахань", 02, 2019));
        dataBase.add(new Employee("Налетов", "Роман", "Андреевич", "Санкт-Петербург", 9, 2013));
        dataBase.add(new Employee("Зайчик", "Петр", "", "Прага", 5, 2018));
        dataBase.add(new Employee("Солонников", "Андрей", "Андреевич", "Тамбов", 9, 2020));
        dataBase.add(new Employee("Пиастро", "Антон", "Олегович", "Дмитров", 1, 2018));
        dataBase.add(new Employee("Кашинцев", "Александр", "Викторович", "Астрахань", 5, 2020));
        dataBase.add(new Employee("Пиастро", "Анна", "Ильинична", "Москва", 9, 2014));
        dataBase.add(new Employee("Мильц", "Томас", "", "Германия", 9, 2018));
        dataBase.add(new Employee("Рабизова", "Светлана", "Игоревна", "Москва", 4, 2003));
        dataBase.add(new Employee("Перова", "Елена", "Александровна", "Тамбов", 6, 2010));
        dataBase.add(new Employee("Давыдова", "Анастасия", "Леонидовна", "Москва", 3, 2011));
        dataBase.add(new Employee("Рюмин", "Роман", "Петрович", "Томск", 8, 2020));

int cmonth = 9;
int cyaer = 2020;

                   for (int i=0; i<dataBase.size(); i++){
                   int month = dataBase.get(i).month;
                    int year = dataBase.get(i).year;
                    int check = ((cyaer-year)*12+(cmonth-month))/12;
                    if (check>=3)
                        System.out.println(dataBase.get(i).toString());

    }
}
}