package com.task;
import java.util.Scanner;

public class TaskCh10N056 {

    public static void main(String[] args) {
        System.out.println("Введите число");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int odd = 3;
       boolean a=true;
        if (n % 2 == 0)
             a = false;
        else {
            while (odd < n) {
                if (n % odd == 0) {
                    a = false;
                    break;
                } else odd = odd + 2;
            }
        }
        //System.out.println(odd);
            if (a==false) System.out.println("Введенное число " + n + " составное");
            else System.out.println("Введенное число " + n + " простое");
   }
}
