package com.task;

import java.util.Scanner;

public class TaskCh12N028 {

    public static void main(String[] args) {
        System.out.println("Введите размер матрицы N*N, где N - нечетное число:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n % 2 != 0) {

           int num = 1;//начало с 1 [0,0]
           int[][] array = new int[n][n];

           for (int i = 0; i < n; i++) {//первая строка 1-5
                array[0][i] = num;
                num++;
            }
            for (int j = 1; j < n; j++) {//последний столбец 5-9
                array[j][n - 1] = num;
                num++;
            }
           for (int i = n - 2; i >= 0; i--) {//последняя строка 9-13
                array[n - 1][i] = num;
                num++;
            }
            for (int j = n - 2; j > 0; j--) {//первый столец 13-16
                array[j][0] = num;
                num++;
            }

            int q = 1;
            int w = 1;

            while (num < n * n) {

                while (array[q][w + 1] == 0) {//вторая строка 17-19
                    array[q][w] = num;
                    num++;
                    w++;
                }

               while (array[q + 1][w] == 0) {//второй столбец справа 19-21
                    array[q][w] = num;
                    num++;
                    q++;
                }

                while (array[q][w - 1] == 0) {//вторая строка снизу 21-23
                    array[q][w] = num;
                    num++;
                    w--;
                }

                while (array[q - 1][w] == 0) {//второй столбец справа 23-24
                    array[q][w] = num;
                    num++;
                    q--;
                }
            }

           for (int x = 0; x < n; x++) {//серединку заполняем
                for (int y = 0; y < n; y++) {
                    if (array[x][y] == 0) {
                        array[x][y] = num;
                    }
                }
            }

           for (int x = 0; x < n; x++) {
                for (int y = 0; y < n; y++) {
                   System.out.print(array[x][y] + " ");

                }
                System.out.println("");
            }
        }
    }
}