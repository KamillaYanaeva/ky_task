package com.task;

import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String[] args) {
        System.out.println("Введите слово");
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        StringBuilder vsword = new StringBuilder(word);

        System.out.println(vsword.reverse());
        }
    }
