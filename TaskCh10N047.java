package com.task;
import java.util.Scanner;

public class TaskCh10N047 {

    static int fibonachi(int k) {

        if (k == 0) {
            return 0;
        }
        if (k == 1) {
            return 1;
        } else {
            return (fibonachi(k - 1) + fibonachi(k - 2));
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите k: ");
        int k = sc.nextInt();

        System.out.println("k-ый член последовательности Фибоначчи равен: "+fibonachi(k));


        }
    }

