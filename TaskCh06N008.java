package com.task;
import java.lang.*;
import java.util.Scanner;

public class TaskCh06N008 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число n:");
        int n = sc.nextInt();
        int i = 1;
        while (i < n) {
            i=i+1;
            System.out.println(Math.pow(i,2));
        }
    }
}