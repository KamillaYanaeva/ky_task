package com.task;
import java.util.Arrays;

public class TaskCh11N245 {

    public static void main(String[] args) {
        int source [] = {-1,25,5,5,1,7,8,-9,0,5,4,6,-7,8,1,1,5,9,3,-555};
        System.out.println("Исходный массив: "+Arrays.toString(source));

        int [] sorted = Arrays.copyOf(source,source.length);
        Arrays.sort(sorted);
        System.out.println("Новый массив: " +Arrays.toString(sorted));
    }
}
