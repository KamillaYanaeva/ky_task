package com.task;
import java.util.Scanner;


public class TaskCh10N043 {
    public static int sumDigits(int a) {
        int sum = 0;

        do {
            sum += a % 10;
            a = a / 10;
        } while (a > 0);

        if (sum >= 10) {
            return sumDigits(sum);
        }
        return sum;
    }
    public static int numDigits(int a) {
        int num = 0;
        for ( ; a != 0 ; a /= 10)
            ++num;

        //do {
          //  num += a % 10;
            //a = a / 10;
        //} while (a > 0);

        //if (num >= 10) {
            //return sumDigits(num);
        //}
        return num;
    }

    public static void main (final String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int a = sc.nextInt();

        double sumDigit = sumDigits(a);
        System.out.println("a) Сумма цифр числа " + sumDigit);
        double numDigit = numDigits(a);
        System.out.println("b) Количество цифр числа " + numDigit);
        }

    }
