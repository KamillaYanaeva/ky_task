package com.task;
import java.util.Scanner;

public class TaskCh10N055 {


   public static void main(String args[])
   {int sn=0;
    int number;
    String result="";
           System.out.println("Введите систему счисления, в которую будем переводить - от 2 до 16");
    Scanner sc=new Scanner(System.in);
       //if (sc.hasNextInt()) sn=sc.nextInt();
       sn=sc.nextInt();
        if (sn<2 || sn>16) {
        System.out.println("Введенное число не соответствует интервалу от 2 до 16");
        return;
    }
        System.out.println("Введите число, которое будете переводить");
       number=sc.nextInt();
    int ost;

       do {
           ost = number % sn;
           if (ost < 10) {
               result = ost + result;
           } else {
               result = (char) ('A' + ost - 10) + result;
           }
           number = number / sn;
       } while (number > 0);
       System.out.print("Результат перевода введенного числа из десятичной системы счисления в " +sn+"-ричную равен " +result);
}
}
