package com.task;
import java.util.Arrays;

public class TaskCh11N158 {

    public static void main(String[] args) {
        int repeat [] = {1,25,5,5,1,7,8,9,5,4,6,7,8,1,1,5,9,3,5};
        System.out.println("Массив с повторяющимися значениями: "+Arrays.toString(repeat));
        int n = repeat.length;

        for ( int i = 0, a = 0; i != n; i++, n = a )
        {
            for ( int j = a = i + 1; j != n; j++ )
            {
                if ( repeat[j] != repeat[i] )
                {
                    if ( a != j ) repeat[a] = repeat[j];
                    a++;
                }
            }
        }
        if ( n != repeat.length )
        {
            int[] worepeat = new int[n];
            for ( int i = 0; i < n; i++ ) worepeat[i] = repeat[i];

            repeat = worepeat   ;
        }

        System.out.println("Массив с повторяющимися значениями: "+Arrays.toString(repeat));

    }
}
