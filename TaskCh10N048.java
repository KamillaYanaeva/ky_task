package com.task;
import java.lang.*;

public class TaskCh10N048 {
    public static int Max(int[] inputArray){
        int max = inputArray[0];
        for(int i=1;i < inputArray.length;i++){ if(inputArray[i] > max){
            max = inputArray[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int array[] = new int[]{5, 11, 0, 222, 1122, 20};

        int max = Max(array);

        System.out.println("Максимальное значение массива: "+max);

    }
}
