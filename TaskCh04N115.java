package com.task;
import java.util.Scanner;

public class TaskCh04N115 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the year number");
        int year = sc.nextInt();
        int animal = year%12;
        int color = year%10;

        String b = "";
        switch (animal) {
            case 4:
                b = "Rat";
                break;
            case 5:
                b = "Ox";
                break;
            case 6:
                b = "Tager";
                break;
            case 7:
                b = "Hare";
                break;
            case 8:
                b = "Dragon";
                break;
            case 9:
                b = "Snake";
                break;
            case 10:
                b = "Horse";
                break;
            case 11:
                b = "Sheep";
                break;
            case 0:
                b = "Monkey";
                break;
            case 1:
                b = "Rooster";
                break;
            case 2:
                b = "Dog";
                break;
            case 3:
                b = "Pig";
                break;

        }

        String a = "";
        switch (color) {
            case 4:
            case 5:
            a = "Green";
                break;
            case 6:
            case 7:
                a = "Red";
                break;
            case 8:
            case 9:
                a = "Yellow";
                break;
            case 0:
            case 1:
                a = "White";
                break;
            case 2:
            case 3:
                a = "Black";
                break;

            }
        System.out.println(b+", "+a);

    }
}
